# Copyright (c) 2024 Giulio Romano De Mattia
#
# MIT License
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in all
# copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.

# Directory containing the .sco file for Csound
CS_DIR = csound
# Directory containing the .sco file for Csound
PY_DIR = python
# Directory containing the .wav file for Reaper
WAV_DIR = plotter/outputWav
# Directory containing the .RPP 
REAPER_PATH = plotter
# Directory containing the csound Libraries - relative path
LIB_RELATIVE_PATH = csound/lib
# Directory containing the csound Libraries - absolute path
LIB_ABS_PATH := $(realpath $(LIB_RELATIVE_PATH))


# Name of the Python script
PY_SCRIPT = main.py
# Name of the .orc file for Csound
ORC_SCRIPT = orc.orc
# Name of the Reaper project file
REAPER_PROJECT = $(REAPER_PATH)/plotter.RPP

# Default target: Generate audio output only if the Python script has produced output
all: generate_sco

# Command to execute the Python script without saving the output in a .sco file
generate_sco:
	@output=$$(python3 $(PY_DIR)/$(PY_SCRIPT)); \
	if [ -z "$$output" ]; then \
		echo "Python well done"; \
		echo "Running Csound..."; \
		make run_csound; \
	else \
		echo "Output from Python script:\n$$output"; \
	fi

# Target to execute Csound on the .sco files
# Find all .sco files and run Csound on each
run_csound:	create_outputWav_folder
	@if [ ! -d "$(CS_DIR)/sco" ]; then \
		echo "La cartella 'sco' non esiste"; \
		exit 1; \
	fi
	@find csound/sco -type f -name "*.sco" -exec sh -c '\
		wav_file="$(WAV_DIR)/$$(basename {} .sco).wav"; \
		csound --env:INCDIR+=$(LIB_ABS_PATH) -o $$wav_file $(CS_DIR)/orc.orc {} || exit $$?' {} \;

# Rule to create the "outputWav" folder if it doesn't exist
create_outputWav_folder:
	@if [ ! -d "$(WAV_DIR)" ]; then \
		mkdir -p "$(WAV_DIR)"; \
		echo "Cartella 'Media' creata in $(WAV_DIR)"; \
	fi

# Target to open REAPER
run:
	@echo "Opening REAPER..."
	open -a "REAPER" "$(REAPER_PROJECT)"
	open $(WAV_DIR)

# Target clean: Remove all files in csound/sco, plotter/Media, and plotter/outputWav
cleanAll:
	rm -f $(CS_DIR)/sco/* $(WAV_DIR)/* $(REAPER_PATH)/Media/*

# Target clean: Remove all files in csound/sco and plotter/outputWav
clean:
	rm -f $(CS_DIR)/sco/* $(WAV_DIR)/*