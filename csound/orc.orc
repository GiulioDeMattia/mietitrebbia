/*
 *  Copyright (c) 2024 Giulio Romano De Mattia
 *
 *  MIT License
 *
 *  Permission is hereby granted, free of charge, to any person obtaining a copy
 *  of this software and associated documentation files (the "Software"), to deal
 *  in the Software without restriction, including without limitation the rights
 *  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 *  copies of the Software, and to permit persons to whom the Software is
 *  furnished to do so, subject to the following conditions:
 *
 *  The above copyright notice and this permission notice shall be included in all
 *  copies or substantial portions of the Software.
 *
 *  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 *  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 *  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 *  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 *  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 *  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 *  SOFTWARE.
 */

sr     = 96000
ksmps  = 32
nchnls = 2
0dbfs  = 1

#include "msPan.lib"
#include "basics.lib"


instr 1
              ; Grains from an input audio file
       idur = p3
       iskiptime = p4
       iamp = p5
       Sfilename = p6
       Sfilename strcat "../material/", Sfilename 
       ktheta = p7
       iwindow = p8
              ; Get the shape of the windows for the grain
       kwindow Window idur,iwindow
       a1 init 0
              ; Get the shape of the incoming audio file
       ichnls filenchnls Sfilename

              ; Use ichnls to determine if the audio file is mono or stereo
       if (ichnls == 2) then
              ; Processing for stereo files
       a1,a2 diskin2 Sfilename, 1, iskiptime, 1
       else
              ; Processing for mono files
       a1 diskin2 Sfilename, 1, iskiptime, 1
              ; Copy the mono signal to both channels
       a2 = a1
       endif
              ; Apply a Mid-Side panning operation using the MsPan opcode       
       aMid, aSide MsPan  a1,ktheta
              ; Apply the Sum-Difference Matrix (Sdmx) to the Mid-Side signals       
       aL,aR Sdmx aMid,aSide
              ; Output audio
       aL = aL*iamp
       aR = aR*iamp
       outs aL*kwindow, aR*kwindow
endin
