"""
Copyright (c) 2024 Giulio Romano De Mattia

MIT License

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
"""

import json
import os
from pydub import AudioSegment
from cloud import Cloud
from gen_segmentGenerator import Gen_segmentGenerator
from gen_window import Gen_window

def get_audio_duration(file_path):
    """
            Get the duration of the audio file in seconds.

            Parameters:
            - file_path (str): Path to the audio file.

            Returns:
            - float: Duration of the audio file in seconds.
    """
    audio = AudioSegment.from_file(file_path)
    duration_in_seconds = len(audio) / 1000
    return duration_in_seconds

def file_exists(file_name, directory, skiptime, amp):
    """
            Check the existence of the audio file and the values of skiptime and amp.

            Parameters:
            - file_name (str): Name of the audio file.
            - directory (str): Directory of the audio file.
            - skiptime (float): Starting point of playback in the audio file in seconds.
            - amp (float): Amplitude, ranging from 0 to 1.

            Returns:
            - str: Full path to the audio file.
    """
    full_path = os.path.join(directory, file_name)
    if not os.path.exists(full_path):
        raise FileNotFoundError(f"The file '{full_path}' does not exist.")

    duration_in_seconds = get_audio_duration(full_path)
    if not (0 <= skiptime <= duration_in_seconds):
        raise ValueError(f"\n\t\tSkiptime ({skiptime}) for the file '{full_path}'\n\t\tmust be between 0 and the duration of the audio file ({duration_in_seconds} sec).\n\n\t\t\t:D you're a forgetful FRIEND\n - \n - \n - \n - \n - ")
    if not (0 <= amp <= 1):
        raise ValueError(f"\n\t\tAmplitude ({amp}) for the file '{full_path}'\n\t\tmust be between 0 and 1.\n\n\t\t\t:D you're a forgetful FRIEND\n - \n - \n - \n - \n - ")

    return full_path

def write_GenTables(sco_directory,Gen):
    # Generate the absolute path for the .sco file based on the iteration index
    sco_file_gen = os.path.join(sco_directory, f"tables.gensco")

    # Open the .sco file and write the Csound details for the cloud
    with open(sco_file_gen, 'a') as sco_file:
        sco_file.write(Gen.toCsound())


def main():
    """
            Execute the main process of the script.

            Read metadata from a JSON file, create instances of Cloud,
            initialize the grains, and write details to .sco files for Csound processing.
    """
    script_directory = os.path.dirname(os.path.abspath(__file__))
    material_directory = os.path.join(script_directory, '../material')
    metadata_cloud = os.path.join(script_directory, '../metadata/clouds.json')
    metadata_tables = os.path.join(script_directory, '../metadata/tables.json')
    sco_directory = os.path.join(script_directory, '../csound', '../csound/sco')

    if not os.path.exists(sco_directory):
        os.makedirs(sco_directory)
    
    with open(metadata_cloud, 'r') as file:
        cloud_list = json.load(file)
    
    try:
        # Iterate through the list of cloud data dictionaries
        for idx, cloud_data in enumerate(cloud_list, start=1):
            # Extract necessary parameters from the dictionary
            file_name = cloud_data.get('fileName')
            skiptime = cloud_data.get('skiptime', 0)
            amp = cloud_data.get('amp', 1.0)

            # Check file existence and validate skiptime and amp parameters
            file_path = file_exists(file_name, material_directory, skiptime, amp)

            # Check if the length of the grain is greater than or equal to the audio file duration
            if skiptime >= get_audio_duration(file_path):
                raise ValueError(f"The length of the grain for the file '{file_path}' is greater than or equal to the duration of the audio file. Please adjust the parameters.")

            # Create an instance of the Cloud class using the data from the dictionary
            cloud = Cloud(**cloud_data)

            # Generate grains for the cloud
            cloud.create()

            # Generate the absolute path for the .sco file based on the iteration index
            sco_file_name = os.path.join(sco_directory, f"output_{idx}.sco")

            # Open the .sco file and write the Csound details for the cloud
            with open(sco_file_name, 'w') as sco_file:
                sco_file.write(cloud.toCsound())

    except Exception as e:
        # Handle any exceptions that might occur during the process
        print(f"Error: {type(e).__name__} - {e}")
        return

    with open(metadata_tables, 'r') as file:
        tables_list = json.load(file)

    try:
        for idx, table_data in enumerate(tables_list, start=1):
            # Extract necessary parameters from the dictionary
            n_GEN = table_data.get('n_GenType')

            if 5 <= n_GEN <= 8:
                # Generate an instance of Gen_segmentGenerator
                gen_instance = Gen_segmentGenerator(n_GEN, table_data.get('ordinate_values'), table_data.get('length_of_segment'))
                write_GenTables(sco_directory,gen_instance)                
            elif n_GEN == 20:
                # Generate an instance of Gen_window
                try:
                    window = int(table_data.get('window'))
                    if 1 <= window <= 9:
                        gen_instance = Gen_window(n_GEN, window)
                        write_GenTables(sco_directory,gen_instance)
                    else:
                        raise ValueError(f"Invalid window value for n_GEN {n_GEN}: {window}. Window must be an integer between 1 and 9.")
                except ValueError:
                    raise ValueError(f"Invalid window value format for n_GEN {n_GEN}. Window must be an integer.")
            else:
                # Raise an exception for an unknown n_GEN value
                raise ValueError(f"Unsupported n_GEN value: {n_GEN}")

    except Exception as e:
        # Handle any exceptions that might occur during the process
        print(f"Error: {type(e).__name__} - {e}")

if __name__ == "__main__":
    main()
