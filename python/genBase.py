class GenBase:
    # Class variable for the function number
    function_number = 2
    # Base scale factor for calculations
    size = 2**14

    def __init__(self, n_GenType):
        """
        Initialize GenBase instance.

        :param time: Time parameter.
        :param size: Size parameter.
        :param n_GenType: Number of GenType parameter.
        """
        self.time = 0
        self.n_GenType = n_GenType
        # Assign the current function number to this instance
        self.function_number = GenBase.get_and_increment_function_number()

    @classmethod
    def get_and_increment_function_number(cls):
        """
        Get the current function number and increment it by 1 for the next instance.

        :return: Current function number.
        """
        current_function_number = cls.function_number
        cls.function_number += 1
        return current_function_number
