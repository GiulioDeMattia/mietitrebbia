"""
Copyright (c) 2024 Giulio Romano De Mattia

MIT License

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
"""

class Grain:
    """
    The `Grano` class represents an individual grain within a Csound composition.

    Attributes:
    - at (float): The point at which the grain starts within the composition in seconds.
    - dur (float): Duration of the grain in seconds.
    - skiptime (float): Point within the audio file to start playing the grain.
    - amp (float): Amplitude of the grain, specified as a value between 0 and 1.
    - nameFile (str): Name of the audio file associated with the grain.

    Methods:
    - toCsound(): Returns a string formatted according to Csound syntax representing the grain.
    """
    def __init__(self, at, dur, skiptime, nameFile, amp, theta,window):
        """
                Initializes a new `Grain` object with the specified parameters.

                Parameters:
                - at (float): The point at which the grain starts within the composition in seconds.
                - dur (float): Duration of the grain in seconds.
                - skiptime (float): Point within the audio file to start playing the grain.
                - amp (float): Amplitude of the grain, specified as a value between 0 and 1.
                - nameFile (str): Name of the audio file associated with the grain.
                - window (int): Type of window associated with the grain.
        """
        self.at = at
        self.dur = dur
        self.skiptime = skiptime
        self.nameFile = nameFile
        self.amp = amp
        self.theta = theta
        self.window = window

    def toCsound(self):
        """
                Returns a string formatted according to Csound syntax representing the grain.

                Returns:
                - str: Csound string representing the grain.
        """
        return f"i1\t{self.at}\t\t{self.dur}\t\t{self.skiptime}\t\t{self.amp}\t\t{self.nameFile}\t\t{self.theta}\t\t{self.window}"
