"""
Copyright (c) 2024 Giulio Romano De Mattia

MIT License

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
"""


from grain import Grain
import math

class Cloud:
    """
            The Cloud class represents a structure for managing the generation
            of sound grains (Grano) based on specific parameters.

            Parameters:
            - at (float): The initial time of the hay bale.
            - dur (float): The total duration of the hay bale.
            - fileName (str): The name of the audio file associated with the hay bale.
            - skiptime (float): The start time when the first grain begins to sound.
            - amp (float): The amplitude of the hay bale.
            - grain_size (float): The size of each sound grain.

            Attributes:
            - at (float): The initial time of the hay bale.
            - dur (float): The total duration of the hay bale.
            - fileName (str): The name of the audio file associated with the hay bale.
            - skiptime (float): The start time when the first grain begins to sound.
            - amp (float): The amplitude of the hay bale.
            - grain_size (float): The size of each sound grain.
            - grains (list): A list of Grano instances associated with the hay bale.
    """

    def __init__(self, at, dur, fileName, skiptime, amp, grain_length,theta,window,overlap):
        self.at = at
        self.dur = dur
        self.fileName = f'"{fileName}"'
        self.skiptime = skiptime
        self.amp = amp
        self.grain_length = grain_length
        self.theta = theta
        self.window = window       
        self.overlap = overlap
        self.hopsize = self.grain_length / self.overlap     
        self.grains = []

    def create(self):
        """
                Calculate the number of grains based on duration and grain_size.
                Calculate necessary values using calculation functions.
                Create grains in the self.grains list.

                The calculated values include 'at' (start time), 'dur' (duration), 
                'skiptime' (skip time), and 'amp' (amplitude).
        """
        # Calculate the number of grains based on duration and grain_size
        self.total_grains = math.ceil((self.dur / self.grain_length) * self.overlap)

        # Calculate values using calculation functions
        at_values = self.calcAt()
        dur_values = self.calcDur()
        skiptime_values = self.calcSkiptime()
        amp_values = self.calcAmp()
        theta_values = self.calcTheta()
        
        # Create grains in the self.grains list
        self.grains = [
            Grain(at=at, dur=dur, skiptime=skiptime, nameFile=self.fileName, amp=amp,theta=theta, window=self.window)
            for at, dur, skiptime, amp, theta in zip(at_values, dur_values, skiptime_values, amp_values,theta_values)
        ]

    def calcAt(self):
        """
                Calculate the 'at' (start time) values for each grain based on the total number of grains,
                the initial time 'self.at', and the grain length 'self.grain_length'.
        """
        return [self.at + i * self.hopsize for i in range(self.total_grains)]

    def calcDur(self):
        """
                Calculate the 'dur' (duration) values for each grain based on the total number of grains
                and the grain length 'self.grain_length'.
        """
        return [self.grain_length for i in range(self.total_grains)]

    def calcSkiptime(self):
        """
                Calculate the 'skiptime' values for each grain based on the total number of grains,
                the initial skip time 'self.skiptime', and the grain length 'self.grain_length'.
        """
        return [self.skiptime + i * self.grain_length for i in range(self.total_grains)]

    def calcAmp(self):
        """
                Calculate the 'amp' (amplitude) values for each grain based on the total number of grains
                and the specified amplitude 'self.amp'.
        """
        return [self.amp for i in range(int(self.total_grains))]

    def calcTheta(self):
        return [self.theta for i in range(int(self.total_grains))]

    def toCsound(self):
        return "#include \"../sco/tables.gensco\"\n;\ta\tdur\tskiptime\tnamefile\tamp\ttheta\twindow\n" + "\n".join([f"{grain.toCsound()}" for grain in self.grains])
