from genBase import GenBase

class Gen_window(GenBase):
    def __init__(self, n_GenType, window):
        """
        Initialize Gen_window instance.
        :param n_GenType: GenType parameter.
        :param window: Window parameter.
        """
        super().__init__(n_GenType)
        self.window = window

    def toCsound(self):
        """
        Convert Gen_window data to Csound format.

        :return: Csound string representation.
        """
        # Customized logic for toCsound method in Gen_window
        # You can add additional logic specific to Gen_window here
        return f"f{self.function_number} {self.time} {GenBase.size} {self.n_GenType} {self.window}\n"
