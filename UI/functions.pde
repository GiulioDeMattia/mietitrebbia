// Copyright (c) 2024 Giulio Romano De Mattia
//
// MIT License
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in all
// copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
// SOFTWARE.



import controlP5.*;
import processing.data.*;
import javax.swing.JFileChooser;
import javax.swing.filechooser.FileNameExtensionFilter;
import java.io.File;

ControlP5 cp5;
String presetName = "MyPreset";
int numColonne = 64;
ArrayList<Colonna> colonne;
boolean dragging = false;
int draggingIndex = -1; // Indice della colonna che stiamo trascinando

void setup() {
  size(512, 300, JAVA2D);
  surface.setTitle("BVILD YVOR OWN GLORY!");

  cp5 = new ControlP5(this);
  addMenuBar();

  colonne = new ArrayList<Colonna>();

  // Calcola la larghezza di ogni colonna in base al numero totale di colonne e allo spazio disponibile
  float larghezzaColonna = width / numColonne;

  // Aggiungi tutte le colonne
  for (int i = 0; i < numColonne; i++) {
    colonne.add(new Colonna(i * larghezzaColonna + larghezzaColonna / 2, height/2, larghezzaColonna,height / 2));
  }

  // Imposta il colore di sfondo
  background(200);
}

void draw() {
  background(200);

  // Disegna le voci di menu
  fill(0);
  textAlign(CENTER, CENTER);
  textSize(14);

  // Disegna tutte le colonne
  for (int i = 0; i < colonne.size(); i++) {
    Colonna colonna = colonne.get(i);
    colonna.display();

    // Controlla se il mouse è sopra la colonna
    if (mouseX > colonna.position.x - colonna.width / 2 && mouseX < colonna.position.x + colonna.width / 2 &&
        mouseY > colonna.position.y - colonna.height / 2 && mouseY < colonna.position.y + colonna.height / 2) {
      colonna.highlight();   
    } else {
      colonna.unhighlight();
    }
  }
  // Se stiamo trascinando, sposta il rettangolo
  if (dragging && draggingIndex != -1) {
    Colonna colonna = colonne.get(draggingIndex);
    colonna.moveRettangolo(mouseY);
   
  }
}



void mouseDragged() {
  // Controlla se il mouse è sopra una colonna
  for (int i = 0; i < colonne.size(); i++) {
    Colonna colonna = colonne.get(i);
    if (mouseX > colonna.position.x - colonna.width / 2 && mouseX < colonna.position.x + colonna.width / 2 &&
        mouseY > colonna.position.y - colonna.height / 2 - 20 && mouseY < colonna.position.y + colonna.height / 2 + 20) {
      // Muovi il rettangolo solo se l'indice della colonna è valido
      if (draggingIndex != -1) {
        float newY = constrain(mouseY, colonna.position.y - colonna.height / 2 + colonna.rettangolo.h / 2 - 20, colonna.position.y + colonna.height / 2 - colonna.rettangolo.h / 2 + 20);
        colonna = colonne.get(draggingIndex);
        colonna.moveRettangolo(newY);
      }
      // Aggiorna l'indice della colonna attuale
      draggingIndex = i;
      return;  // Esci dalla funzione dopo aver trovato la colonna corrispondente
    }
  }
}

void mouseReleased() {
  // Interrompi il trascinamento quando rilasci il mouse
  dragging = false;
  draggingIndex = -1;
}



// Aggiungi la barra dei menu utilizzando ControlP5
void addMenuBar() {
  cp5.addButton("caricaPreset")
     .setPosition(5, 0)
     .setSize(80, 30)
     .setLabel("Carica Preset");

  cp5.addButton("salvaPreset")
     .setPosition(90, 0)
     .setSize(80, 30)
     .setLabel("Salva Preset");
}

void fileSelected(File selection) {
  if (selection == null) {
    println("Nessun file selezionato.");
  } else {
    // Cliccato su "Carica Preset" e selezionato un file
    loadPreset(selection.getAbsolutePath());
  }
}


String presetFileName;

void salvaPreset() {
  // Apre la finestra di dialogo per salvare il file
  selectOutput("Salva come:", "savePresetSelected");

  // La funzione savePresetSelected verrà chiamata quando l'utente seleziona un file
}

// Questa funzione viene chiamata quando l'utente seleziona un file
void savePresetSelected(File selection) {
  if (selection == null) {
    println("Nessun file selezionato.");
  } else {
    // Ottieni il percorso assoluto del file selezionato
    presetFileName = selection.getAbsolutePath();

    // Aggiungi automaticamente l'estensione .json al nome del file se non è già presente
    if (!presetFileName.toLowerCase().endsWith(".json")) {
      presetFileName += ".json";
    }

    
    // Chiamata alla funzione per effettuare il salvataggio del preset
    savePreset(presetFileName);
  }
}


// Funzione richiamata dopo la selezione del file da caricare
void caricaPreset() {
  // Apre la finestra di dialogo per la selezione del file da caricare
  selectInput("Seleziona un file da caricare", "loadPresetSelected");
}

// Questa funzione viene chiamata quando l'utente seleziona un file da caricare
void loadPresetSelected(File selection) {
  if (selection == null) {
    println("Nessun file selezionato.");
  } else {
    // Ottieni il percorso assoluto del file selezionato
    String presetFileName = selection.getAbsolutePath();
    
    // Chiamata alla funzione per effettuare il caricamento del preset
    loadPreset(presetFileName);
  }
}


// Funzione per salvare il preset
void savePreset(String fileName) {
  JSONObject preset = new JSONObject();
  preset.setString("preset_name", presetName); // Personalizza il nome del preset se necessario

  JSONArray rectangles = new JSONArray();
  for (int i = 0; i < colonne.size(); i++) {
    Colonna colonna = colonne.get(i);
    JSONObject rectInfo = new JSONObject();
    rectInfo.setFloat("x", colonna.rettangolo.position.x);
    rectInfo.setFloat("y", colonna.rettangolo.position.y);
    rectInfo.setFloat("w", colonna.rettangolo.w);
    rectInfo.setFloat("h", colonna.rettangolo.h);
    rectangles.setJSONObject(i, rectInfo);
  }

  preset.setJSONArray("rectangles", rectangles);
  saveJSONObject(preset, fileName);
}

// Funzione per caricare il preset
void loadPreset(String fileName) {
  JSONObject preset = loadJSONObject(fileName);
  if (preset != null) {
    // Recupera la stringa preset_name se necessario
    String loadedPresetName = preset.getString("preset_name", "Untitled");
    println("Loading Preset: " + loadedPresetName);

    // Recupera l'array di rettangoli
    JSONArray rectangles = preset.getJSONArray("rectangles");
    for (int i = 0; i < colonne.size() && i < rectangles.size(); i++) {
      Colonna colonna = colonne.get(i);
      JSONObject rectInfo = rectangles.getJSONObject(i);
      float x = rectInfo.getFloat("x");
      float y = rectInfo.getFloat("y");
      float w = rectInfo.getFloat("w");
      float h = rectInfo.getFloat("h");
      colonna.rettangolo.position.set(x, y);
      colonna.rettangolo.w = w;
      colonna.rettangolo.h = h;
    }
  } else {
    println("Error loading preset: " + fileName);
  }
}

// Funzione di utilità per la selezione della cartella
String selectFolder(String prompt) {
  JFileChooser fileChooser = new JFileChooser();
  fileChooser.setFileSelectionMode(JFileChooser.DIRECTORIES_ONLY);
  fileChooser.setDialogTitle(prompt);

  int result = fileChooser.showOpenDialog(null);
  if (result == JFileChooser.APPROVE_OPTION) {
    File selectedFolder = fileChooser.getSelectedFile();
    return selectedFolder.getAbsolutePath();
  } else {
    return null;
  }
}


// Classe che rappresenta un oggetto colonna
class Colonna {
  PVector position;
  float width;
  float height;
  Rettangolo rettangolo; // Ogni colonna contiene un solo rettangolo

  // Costruttore della colonna, riceve la posizione iniziale, la larghezza e l'altezza
  Colonna(float x, float y, float w, float h) {
    position = new PVector(x, y);
    width = w;
    height = h;
    rettangolo = new Rettangolo(position.x, position.y, width, height / 50); // Il rettangolo è posizionato al centro della colonna
  }

  void startDrag() {
  rettangolo.startDrag();
  }
  
  void stopDrag() {
  rettangolo.stopDrag();
  }
  
  // Metodo per visualizzare la colonna
  void display() {
    // Disegna la colonna
    rectMode(CENTER);
    fill(0);
    noStroke();
    rect(position.x, position.y, width, height);

    // Disegna il rettangolo nella colonna
    rettangolo.display();
  }

  // Evidenzia la colonna quando il mouse è sopra
  void highlight() {
    fill(255, 0, 0, 100);
    rect(position.x, position.y, width, height);
  }

  // Rimuovi l'evidenziazione della colonna
  void unhighlight() {
    noFill();
    rect(position.x, position.y, width, height);
  }

  // Sposta il rettangolo lungo l'altezza della colonna
  void moveRettangolo(float y) {
    rettangolo.position.y = constrain(y, position.y - height / 2 + rettangolo.h / 2, position.y + height / 2 - rettangolo.h / 2);
  }
}

// Classe che rappresenta un oggetto rettangolo
class Rettangolo {
  PVector position;
  float w, h;

  // Costruttore del rettangolo, riceve la posizione, la larghezza e l'altezza
  Rettangolo(float x, float y, float w, float h) {
    position = new PVector(x, y);
    this.w = w;
    this.h = h;
  }

  // Metodo per visualizzare il rettangolo
  void display() {
    // Disegna il rettangolo
    rectMode(CENTER);
    fill(255);
    noStroke();
    rect(position.x, position.y, w, h);
  }
  
  // All'interno della classe Rettangolo
  void startDrag() {
    dragging = true;
  }
  
  // All'interno della classe Rettangolo
  void stopDrag() {
    dragging = false;
  }

}
